
/**
 * Renders a products
 * @param product 
 * @returns a new html element representing the given product
 */
function createProductElement(product) {

    // create image element
    const imageElement = document.createElement('img');
    imageElement.classList.add('product-image');
    imageElement.setAttribute('alt', 'Product image');
    imageElement.setAttribute('src', product.image);

    // create title element
    const titleElement = document.createElement('h2');
    titleElement.classList.add('product-title');
    titleElement.innerText = product.title;

    // create description element
    const descriptionElement = document.createElement('p');
    descriptionElement.classList.add('product-description');
    descriptionElement.innerText = product.description;

    const textGroupElement = document.createElement('div');
    textGroupElement.appendChild(titleElement);
    textGroupElement.appendChild(descriptionElement);

    // create price element
    const priceElement = document.createElement('p');
    priceElement.classList.add('product-price');
    priceElement.innerText = `${product.price}€`;

    // create content-wrapper element'
    const contentWrapper = document.createElement('div');
    contentWrapper.classList.add('content-wrapper');
    contentWrapper.appendChild(textGroupElement);
    contentWrapper.appendChild(priceElement);

    // create product element
    const productElement = document.createElement('div');
    productElement.classList.add('product');
    productElement.appendChild(imageElement);
    productElement.appendChild(contentWrapper);

    return productElement;
}

/**
 * Renders a list of products
 * @param productList
 */
function renderProducts(productList) {

    const listElement = document.getElementById('product_list');

    // iterate all items
    for (let i = 0; i < productList.length; i++) {
        
        const product = productList[i];
        const productElement = createProductElement(product);
        listElement.appendChild(productElement);
    }
}

/**
 * Fetches all products
 * @returns Promise
 */
function fetchProducts() {

    function executor(resolve, reject) {

        fetch('https://fakestoreapi.com/products/')
            .then(resp => resp.json())
            .then(json => resolve(json))
            .catch(error => reject(error));
    }

    return new Promise(executor);
}

/**
 * This ist the entry point of the applications
 */
function init() {
    fetchProducts()
    .then(products => {
        renderProducts(products);
    })
}

// Call the entrypoint when script is loaded
init();